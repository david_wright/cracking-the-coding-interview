#ifndef TRAINING_UTIL_HPP
#define TRAINING_UTIL_HPP
#include <boost/core/demangle.hpp>
#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <vector>

namespace tree {
template<typename T>
struct Node {
	std::vector<std::shared_ptr<Node<T>>> children;
	T data;
};

template<typename T>
struct Tree {
	std::shared_ptr<Node<T>> root;
};

template<typename T>
struct AdjacencyData {
	T data;
	std::vector<uint32_t> children;
};

template<typename T>
using AdjacencyList = std::vector<AdjacencyData<T>>;

template<typename T>
Tree<T> createTree(AdjacencyList<T> &adjacencyList) {
	Tree<T> tree;
	std::vector<std::shared_ptr<Node<T>>> nodes;

	for (uint32_t i = 0; i < adjacencyList.size(); i++) {
		nodes.emplace_back(std::make_shared<Node<T>>());
	}

	for (uint32_t i = 0; i < adjacencyList.size(); i++) {
		nodes[i]->data = adjacencyList[i].data;

		for (const auto &child : adjacencyList[i].children) {
			if (child >= nodes.size()) {
				throw std::runtime_error("Requested child node not provided");
			}

			nodes[i]->children.push_back(nodes[child]);
		}
	}

	tree.root = nodes[0];
	return tree;
}

namespace log_internal {
template<typename T>
struct NodeLogInfo {
	std::string prefix;
	const Node<T> &node;
};
} // namespace log_internal
} // namespace tree

namespace graph {
template<typename T>
struct Node {
	std::vector<std::shared_ptr<Node<T>>> children;
	T data;
};

template<typename T>
struct Graph {
	std::vector<std::shared_ptr<Node<T>>> nodes;
};

template<typename T>
struct AdjacencyData {
	T data;
	std::vector<uint32_t> children;
};

template<typename T>
using AdjacencyList = std::vector<AdjacencyData<T>>;

template<typename T>
Graph<T> createGraph(AdjacencyList<T> &adjacencyList) {
	Graph<T> graph;

	for (uint32_t i = 0; i < adjacencyList.size(); i++) {
		graph.nodes.emplace_back(std::make_shared<Node<T>>());
	}

	for (uint32_t i = 0; i < adjacencyList.size(); i++) {
		graph.nodes[i]->data = adjacencyList[i].data;

		for (const auto &child : adjacencyList[i].children) {
			if (child >= graph.nodes.size()) {
				throw std::runtime_error("Requested child node not provided");
			}

			graph.nodes[i]->children.push_back(graph.nodes[child]);
		}
	}

	return graph;
}
} // namespace graph

// TODO (WRD): forward declare all ostream overloads

template<typename T>
std::ostream &operator<<(std::ostream &os, const tree::AdjacencyData<T> &n);
template<typename T>
std::ostream &operator<<(std::ostream &os, const graph::AdjacencyData<T> &n);
template<typename T>
std::ostream &operator<<(std::ostream &os, const tree::log_internal::NodeLogInfo<T> &n);
template<typename T>
std::ostream &operator<<(std::ostream &os, const tree::Node<T> &n);
template<typename T>
std::ostream &operator<<(std::ostream &os, const tree::Tree<T> &t);
template<typename T1, typename T2>
std::ostream &operator<<(std::ostream &os, const std::pair<T1, T2> &p);
template<typename T>
std::ostream &operator<<(std::ostream &os, const std::vector<T> &v);
template<typename... Args>
std::ostream &operator<<(std::ostream &os, const std::tuple<Args...> tup);

template<typename T>
std::ostream &operator<<(std::ostream &os, const tree::AdjacencyData<T> &n) {
	os << "{data: " << n.data << ", children: " << n.children << "}";

	return os;
};

template<typename T>
std::ostream &operator<<(std::ostream &os, const graph::AdjacencyData<T> &n) {
	os << "{data: " << n.data << ", children: " << n.children << "}";

	return os;
};

template<typename T>
std::ostream &operator<<(std::ostream &os, const tree::log_internal::NodeLogInfo<T> &n) {
	if (n.node.children.size() > 0) {
		for (auto child = n.node.children.begin(); child < n.node.children.end() - 1; child++) {
			os << n.prefix << "├───" << (*child)->data << std::endl;
			os << tree::log_internal::NodeLogInfo<T>{n.prefix + "|   ", *(*child)};
		}

		os << n.prefix << "└───" << n.node.children.back()->data << std::endl;
		os << tree::log_internal::NodeLogInfo<T>{n.prefix + "    ", *(n.node.children.back())};
	}

	return os;
};

template<typename T>
std::ostream &operator<<(std::ostream &os, const tree::Node<T> &n) {
	os << n.data << std::endl;
	os << tree::log_internal::NodeLogInfo<T>{"", n};
	return os;
};

template<typename T>
std::ostream &operator<<(std::ostream &os, const tree::Tree<T> &t) {
	os << *(t.root);

	os << std::endl;

	return os;
};

template<typename T1, typename T2>
std::ostream &operator<<(std::ostream &os, const std::pair<T1, T2> &p) {
	os << "(" << p.first << ", " << p.second << ")";
	return os;
};

template<typename T>
std::ostream &operator<<(std::ostream &os, const std::vector<T> &v) {
	os << "[";

	for (auto element = v.begin(); element < v.end(); element++) {
		os << *element;
		if (element < v.end() - 1) {
			os << ", ";
		}
	}

	os << "]";

	return os;
};

template<typename... Args>
std::ostream &operator<<(std::ostream &os, const std::tuple<Args...> tup) {
	int idx = 0;

	std::apply(
		[&os](Args const &...tupleArgs) {
			os << '<';
			std::size_t n{0};
			((os << tupleArgs << (++n != sizeof...(Args) ? ", " : "")), ...);
			os << '>';
		},
		tup);
	return os;
};

template<typename FUN, typename DATA>
bool test(FUN &&fun, DATA &data) {
	int failures = 0;

	std::cout << std::string(100, '=') << std::endl;

	for (auto &d : data) {
		std::cout << "Testing input:\t\t" << d.first << std::endl;

		const auto ret = fun(d.first);
		failures += ret != d.second;

		std::cout << "expected:\t\t\t" << d.second << std::endl << "actual:\t\t\t\t" << ret << std::endl << std::endl;
	}

	return failures == 0;
}

#define REGISTER_TEST(TEST_NAME, TEST_FUN, TEST_DATA) \
	TEST(CrackingTest, TEST_NAME) {                   \
		EXPECT_TRUE(test(TEST_FUN, TEST_DATA));       \
	}

#endif // TRAINING_UTIL_HPP
