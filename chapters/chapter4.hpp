#ifndef TRAINING_CHAPTER4_HPP
#define TRAINING_CHAPTER4_HPP
#include "../util.hpp"

#include <queue>

namespace q4_1 {
using Ret   = bool;
using Param = std::pair<graph::AdjacencyList<uint32_t>, std::pair<uint32_t, uint32_t>>;
using Data  = std::vector<std::pair<Param, Ret>>;

Ret breadthFirstSearchForPathBetween(Param &p) {
	auto graph      = graph::createGraph(p.first);
	const auto from = graph.nodes[p.second.first];
	const auto to   = graph.nodes[p.second.second];

	std::map<std::shared_ptr<graph::Node<uint32_t>>, bool> visited;
	std::queue<std::shared_ptr<graph::Node<uint32_t>>> queue;

	visited[from] = true;
	queue.push(from);

	while (!queue.empty()) {
		const auto node = queue.front();
		queue.pop();

		if (node == to) {
			return true;
		}

		for (const auto &child : node->children) {
			if (!visited.contains(child)) {
				visited[child] = true;
				queue.push(child);
			}
		}
	}

	return false;
}

Data data = {{{{{0, {1, 2}}, {1, {2, 3, 4}}, {2, {}}, {3, {}}, {4, {}}}, {0, 4}}, true},
	{{{{0, {1}}, {1, {}}, {2, {}}}, {0, 2}}, false}, {{{{0, {1, 2}}, {1, {}}, {2, {}}}, {0, 0}}, true},
	{{{{0, {1, 2}}, {1, {}}, {2, {}}}, {2, 0}}, false}};

REGISTER_TEST(Q4_1PathBetween, breadthFirstSearchForPathBetween, data);
} // namespace q4_1

namespace q4_7 {
using Ret   = bool;
using Param = graph::AdjacencyList<uint32_t>;
using Data  = std::vector<std::pair<Param, Ret>>;

Ret testGraph(Param &p) {
	auto tree = graph::createGraph(p);

	return true;
}

Data data = {{{{0, {1, 2}}, {1, {3, 4}}, {2, {5, 6}}, {3, {}}, {4, {}}, {5, {}}, {6, {}}}, true}};

REGISTER_TEST(Q4_1TestTree, testGraph, data);
} // namespace q4_7

#endif // TRAINING_CHAPTER4_HPP
