#ifndef TRAINING_CHAPTER1_HPP
#define TRAINING_CHAPTER1_HPP
#include "../util.hpp"

#include <algorithm>
#include <array>
#include <gtest/gtest.h>
#include <sstream>
#include <tuple>

namespace q1_1 {
using Ret   = bool;
using Param = std::string_view;
using Data  = std::vector<std::pair<Param, Ret>>;

Ret hasAllUniqueChars(Param &p) {
	std::map<char, bool> charMap;

	for (const auto c : p) {
		if (charMap.find(c) == charMap.end()) {
			charMap[c] = true;
		}
		else {
			return false;
		}
	}

	return true;
}

Data data = {{"asdf", true}, {"foo", false}, {"bar", true}, {"11111", false}, {"123", true}};

REGISTER_TEST(Q1_1StringHasAllUniqueChars, hasAllUniqueChars, data);
} // namespace q1_1

namespace q1_2 {
using Ret   = bool;
using Param = std::pair<std::string_view, std::string_view>;
using Data  = std::vector<std::pair<Param, Ret>>;

Ret isPermutation(Param &p) {
	std::map<char, int> charMap1;
	std::map<char, int> charMap2;

	if (p.first.size() != p.second.size()) {
		return false;
	}

	for (int i = 0; i < p.first.size(); i++) {
		const auto c1 = p.first[i];

		if (charMap1.find(c1) == charMap1.end()) {
			charMap1[c1] = 0;
		}
		else {
			charMap1[c1]++;
		}

		const auto c2 = p.second[i];
		if (charMap2.find(c2) == charMap2.end()) {
			charMap2[c2] = 0;
		}
		else {
			charMap2[c2]++;
		}
	}

	for (int i = 0; i < p.first.size(); i++) {
		const auto c = p.first[i];

		if (charMap1[c] != charMap2[c]) {
			return false;
		}
	}

	return true;
}

Data data = {{{"asdf", "fdsa"}, true}, {{"1111", "11"}, false}, {{"1212", "1122"}, true}};

REGISTER_TEST(Q1_2StringIsPermutation, isPermutation, data);
} // namespace q1_2

namespace q1_3 {
using Param = std::string_view;
using Ret   = std::string;
using Data  = std::vector<std::pair<Param, Ret>>;

Ret urlify(Param &p) {
	Ret ret = "";
	ret.reserve(p.size());

	for (const auto &c : p) {
		if (c == ' ') {
			ret.append("\%20");
		}
		else {
			ret.append(1, c);
		}
	}

	return ret;
}

Data data = {{"Mr John Smith", "Mr\%20John\%20Smith"}};
REGISTER_TEST(Q1_3URLify, urlify, data);
} // namespace q1_3

namespace q1_4 {

using Param = std::string;
using Ret   = bool;
using Data  = std::vector<std::pair<Param, Ret>>;

Ret isPalindromePermutation(Param &p) {
	std::map<char, int> charMap;
	auto param = p;

	param.erase(std::remove_if(param.begin(), param.end(),
					[](const char &c) {
						return std::isspace(c);
					}),
		param.end());

	std::transform(param.begin(), param.end(), param.begin(), [](const char c) {
		return std::tolower(c);
	});

	for (const auto &c : param) {
		if (charMap.find(c) == charMap.end()) {
			charMap[c] = 1;
		}
		else {
			charMap[c]++;
		}
	}

	bool previousOddFound = false;
	for (const auto &c : charMap) {
		if (c.second % 2 != 0) {
			if (previousOddFound) {
				return false;
			}
			else {
				previousOddFound = true;
			}
		}
	}

	return true;
}

Data data = {{"Mr John Smith", false}, {"taco cat", true}, {"aa", true}, {"aaa", true}, {"aab", true}, {"aaab", false},
	{"abb a", true}, {"foo         ", true}};
REGISTER_TEST(Q1_4IsPalindromePermutation, isPalindromePermutation, data);
} // namespace q1_4

namespace q1_5 {
using Param = std::pair<std::string_view, std::string_view>;
using Ret   = bool;
using Data  = std::vector<std::pair<Param, Ret>>;

Ret editsAway(Param &p) {
	if (p.first.length() == p.second.length()) {
		bool previousDiffFound = false;

		for (size_t i = 0; i < p.first.length(); i++) {
			if (p.first[i] != p.second[i]) {
				if (previousDiffFound) {
					return false;
				}
				else {
					previousDiffFound = true;
				}
			}
		}
	}
	else if (std::abs(static_cast<int32_t>(p.first.length()) - static_cast<int32_t>(p.second.length())) == 1) {
		const std::string_view *shorter = nullptr;
		const std::string_view *longer  = nullptr;

		if (p.first.length() > p.second.length()) {
			longer  = &p.first;
			shorter = &p.second;
		}
		else {
			longer  = &p.second;
			shorter = &p.first;
		}

		bool previousDiffFound = false;
		size_t shorterIndex    = 0;
		size_t longerIndex     = 0;

		while (shorterIndex < shorter->length() && longerIndex < longer->length()) {
			if (shorter->operator[](shorterIndex) != longer->operator[](longerIndex)) {
				if (previousDiffFound) {
					return false;
				}
				else {
					previousDiffFound = true;
					longerIndex++;
				}
			}
			else {
				shorterIndex++;
				longerIndex++;
			}
		}
	}
	else {
		return false;
	}
	return true;
}

Data data = {{{"Mr John Smith", "Ms John Smith"}, true}, {{"taco cat", "tacoo  cat"}, false}, {{"apple", "aple"}, true},
	{{"aple", "apple"}, true}, {{"aple", "ape"}, true}, {{"appe", "apple"}, true}, {{"apple", "appleeeeeee"}, false}};

REGISTER_TEST(Q1_5EditsAway, editsAway, data);
} // namespace q1_5

namespace q1_6 {
using Param = std::string_view;
using Ret   = std::string;
using Data  = std::vector<std::pair<Param, Ret>>;

Ret strCompression(Param &p) {
	std::stringstream ret;

	auto n              = 1;
	auto mostRecentChar = p[0];

	for (auto c = p.begin() + 1; c < p.end(); c++) {
		if (*c != mostRecentChar) {
			ret << mostRecentChar << n;

			mostRecentChar = *c;
			n              = 1;
		}
		else {
			n++;
		}
	}

	ret << mostRecentChar << n;

	if (ret.str().size() >= p.size()) {
		return std::string(p);
	}
	else {
		return ret.str();
	}
}

Data data = {{"aabcDzzYy//", "aabcDzzYy//"}, {"aabcDDDDDzzzzYYYYYyy//", "a2b1c1D5z4Y5y2/2"},
	{"aabcDDDDDzzzzYYYYYyy/", "a2b1c1D5z4Y5y2/1"}};

REGISTER_TEST(Q1_6StrCompression, strCompression, data);
} // namespace q1_6

namespace q1_7 {
using Param = std::vector<std::vector<int>>;
using Ret   = std::vector<std::vector<int>>;
using Data  = std::vector<std::pair<Param, Ret>>;

std::tuple<std::pair<int, int>, std::pair<int, int>, std::pair<int, int>, std::pair<int, int>> coordinateTransform(
	const int row, const int col, const int dim) {
	const auto topLeft     = std::pair{row, col};
	const auto topRight    = std::pair{col, dim - 1 - row};
	const auto bottomRight = std::pair{dim - 1 - row, dim - 1 - col};
	const auto bottomLeft  = std::pair{dim - 1 - col, row};
	return std::make_tuple(topLeft, topRight, bottomRight, bottomLeft);
}

Ret inPlaceMatrixRotation(Param &p) {
	if (p.size() != p[0].size()) {
		throw std::runtime_error("Q1_7MatrixRotation: Dimensions should be equal");
	}

	auto numCols = p.size() % 2 == 0 ? p.size() / 2 : p.size() / 2 + 1;
	auto numRows = p[0].size() / 2;

	for (auto row = 0; row < numRows; row++) {
		for (auto col = 0; col < numCols; col++) {
			const auto [topLeft, topRight, bottomRight, bottomLeft] = coordinateTransform(row, col, p.size());

			const auto tmp                           = p[topLeft.first][topLeft.second];
			p[topLeft.first][topLeft.second]         = p[topRight.first][topRight.second];
			p[topRight.first][topRight.second]       = p[bottomRight.first][bottomRight.second];
			p[bottomRight.first][bottomRight.second] = p[bottomLeft.first][bottomLeft.second];
			p[bottomLeft.first][bottomLeft.second]   = tmp;
		}
	}

	return p;
}

Data data = {{{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}, {{3, 6, 9}, {2, 5, 8}, {1, 4, 7}}},
	{{{0, 1, 2, 3}, {4, 5, 6, 7}, {8, 9, 10, 11}, {12, 13, 14, 15}},
		{{3, 7, 11, 15}, {2, 6, 10, 14}, {1, 5, 9, 13}, {0, 4, 8, 12}}},
	{{{0, 1}, {2, 3}}, {{1, 3}, {0, 2}}},
	{{{0, 1, 2, 3, 4}, {5, 6, 7, 8, 9}, {10, 11, 12, 13, 14}, {15, 16, 17, 18, 19}, {20, 21, 22, 23, 24}},
		{{4, 9, 14, 19, 24}, {3, 8, 13, 18, 23}, {2, 7, 12, 17, 22}, {1, 6, 11, 16, 21}, {0, 5, 10, 15, 20}}}};

REGISTER_TEST(Q1_7InPlaceMatrixRotation, inPlaceMatrixRotation, data);
} // namespace q1_7

namespace q1_8 {
using Param = std::vector<std::vector<int>>;
using Ret   = std::vector<std::vector<int>>;
using Data  = std::vector<std::pair<Param, Ret>>;

Ret zeroMatrix(Param &p) {
	Ret ret(p.size(), std::vector<int>(p[0].size()));

	auto zeroCols = std::vector<bool>(p[0].size(), false);
	for (auto row = 0; row < p.size(); row++) {
		bool rowIsZero = false;
		for (auto col = 0; col < p[0].size(); col++) {
			if (p[row][col] == 0) {
				ret[row][col] = 0;
				rowIsZero     = true;
				zeroCols[col] = true;

				for (auto rowTmp = 0; rowTmp < row; rowTmp++) {
					ret[rowTmp][col] = 0;
				}

				for (auto colTmp = 0; colTmp < col; colTmp++) {
					ret[row][colTmp] = 0;
				}
			}
			else {
				if (rowIsZero || zeroCols[col]) {
					ret[row][col] = 0;
				}
				else {
					ret[row][col] = p[row][col];
				}
			}
		}
	}

	return ret;
}

Data data = {{{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}, {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}},
	{{{0, 2, 3}, {4, 5, 6}, {7, 8, 9}}, {{0, 0, 0}, {0, 5, 6}, {0, 8, 9}}},
	{{{0, 0, 0}, {4, 5, 6}, {7, 8, 9}}, {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}}},
	{{{0, 1, 2}, {3, 0, 5}, {6, 7, 0}}, {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}}}};

REGISTER_TEST(Q1_8ZeroMatrix, zeroMatrix, data);
} // namespace q1_8

namespace q1_9_1 {
using Param = std::pair<const std::string, const std::string>;
using Ret   = bool;
using Data  = std::vector<std::pair<Param, Ret>>;

Ret isSubstring(const std::string_view sub, const std::string_view whole) {
	if (sub.size() > whole.size()) {
		return false;
	}

	auto wholePos = whole.begin();
	auto subPos   = sub.begin();

	do {
		if (*wholePos != *subPos) {
			wholePos++;
			subPos = sub.begin();
		}
		else {
			wholePos++;
			subPos++;
		}
	} while (wholePos < whole.end() && subPos < sub.end());

	return subPos == sub.end();
}

Ret isSubstringWrapper(Param &p) {
	return isSubstring(p.first, p.second);
}

Data data = {{{"fo", "foo"}, true}, {{"oo", "foo"}, true}, {{"moo", "foomooboo"}, true}, {{"moo", "foo"}, false},
	{{"bar", "b"}, false}, {{"foo", "foo"}, true}};

REGISTER_TEST(Q1_9_1IsSubstring, isSubstringWrapper, data);
} // namespace q1_9_1

namespace q1_9 {
using Param = std::pair<std::string, std::string>;
using Ret   = bool;
using Data  = std::vector<std::pair<Param, Ret>>;

Ret isStringRotation(Param &p) {
	std::reverse(p.first.begin(), p.first.end());

	return (p.first.size() == p.second.size()) && q1_9_1::isSubstring(p.first, p.second);
}

Data data = {{{"foo", "foo"}, false}, {{"oof", "foo"}, true}, {{"ofo", "ofo"}, true}};

REGISTER_TEST(Q1_9StringRotation, isStringRotation, data);
} // namespace q1_9

#endif // TRAINING_CHAPTER1_HPP
